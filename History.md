
n.n.n / 2018-08-06
==================

  * CI: Remove it due to slowness and flakiness
  * Create SRLI Chisel flat spec tests for decoder
  * Create OR Chisel flat spec tests for decoder
  * Create XOR Chisel flat spec tests for decoder
  * It fixes the issue of SRLI execution (#4)
  * fix last commit
  * Create SLTU Chisel flat spec tests for decoder
  * CI: Fix typo
  * Create SLT Chisel flat spec tests for decoder
  * CI: Store artifacts of each job
  * CI: Only install RISC-V toolchain when running ITs
  * CI: Install build-essential and deps
  * CI: Fix typo
  * Improve CI script
  * Delete travis config
  * Add GitLab CI file
  * Change Adept-TestFiles URL
  * Create ANDI Chisel flat spec tests for decoder
  * Create SLL Chisel flat spec tests for decoder
  * Create ORI Chisel flat spec tests for decoder
  * Create XORI Chisel flat spec tests for decoder
  * Create SLTIU Chisel flat spec tests for decoder
  * Creation of chisel spec tests for SUB
  * Creation of chisel spec tests for ADD
  * Create SLTI Chisel flat spec tests for decoder
  * Create ADDI Chisel flat spec tests for decoder
  * Travis: Fix path to obtain RISC-V toolchain
  * Test: Print current instruction test
  * Bump Adept TestFiles
  * Tests: Improve run instruction tests script
  * Compare the same types in Memory Tests
  * Bump test files
  * Fix typo
  * Fixes SLLI, and adds tests
  * Connect trap to high when opcode is invalid
  * Use an Enum to select operand B to the ALU
  * Use enums for the core's control signals
  * Use an enum for Memory operations
  * Fix indentation
  * Change idecode to decoder package
  * Make decoder generation completely dynamic
  * Improve variable names
  * Remove dead BranchOpConstants class
  * Update tests to make use of the PcOps enum
  * Use an enum for Pc ops
  * Simplify ALU interface and decoder control signals
  * Update tests to make use of ALU Ops Enum
  * ALU Ops are an enumerate
  * Travis: Don't send artifacts after build
  * Connect Trap signal to Top Module
  * Improve MUX readability
  * Makefile: Delete everything with clean-verilog
  * When trap occurs also dump registers and PC
  * Make connectDecoders generic
  * Clone the parent's output interface
  * Travis: Build simulation model
  * Move I set instructions to their folder
  * Bump Chisel version
  * Perform final decoder connections and connect trap
  * Pass the Control instruction op to the IF stage
  * Add JAL decode implementation
  * Add LUI decode implementation
  * Add AUIPC decode implementation
  * Fix bugs in InstructionControlSignals class
  * Add branches type instructions impls to decoder
  * Add store type instructions impls to decoder
  * Start killing old code
  * Add register type instructions impls to decoder
  * Add load type instructions impls to the decoder
  * Add jalr instruction implementation to the decoder
  * Add immediate type instructions decoder impl
  * Make control signals drop to a default value
  * Set all signals in decoding bundles to DontCare
  * Bundle PC decode signals
  * Remove comment
  * By default testing the core runs in simulation
  * Move Decoding IO to their respective modules
  * Parse command line arguments to generate a config
  * Add verbosity level to simulations
  * Bump test files
  * Deprecate FIRRTL simulation for the core
  * Change Chisel memory model to Verilog model
  * Modify verbosity settings to comply with tests
  * Add verbosity flag
  * Remove echos
  * Script QoL improvements
  * Travis: send artifacts print to stdout to avoid TO
  * Add travis before_install script
  * Update travis path
  * Travis: Small Lints
  * Travis: go up a dir when installing the toolchain
  * Disable host public key check
  * Fix scp port flag
  * Copy RISCV toolchain from our servers
  * Only send artifacts in master and not a PR
  * Write to the screen manually to avoid travis TO
  * Move artifact creation to its script
  * Move travis_wait to the install script
  * Upload artifacts to our servers
  * Increase travis timeout
  * Add the RISCV toolchain to the dependencies script
  * Make test fail when output comparison fails
  * Build dependencies from script
  * Update Adept Test Files
  * Fix comment in simulation end
  * Update README.md
  * Update README.md
  * Update README.md
  * Update README.md
  * Build specific version of verilator
  * Add test framework
  * Fix JALR Instruction
  * PC: Refactor parts of the Module
  * PC: More refactoring in the PC tests
  * PC: Refactor Branch tests
  * PC: Remove old test
  * PC: Add tests for JALR instructions
  * PC: Add tests for JAL instruction
  * Add team's email addresses to travis configuration
  * PC: Fix bug in test:PcMain
  * PC: Add tests for BGEU instructions
  * PC: Add tests for BLTU instructions
  * PC: Add tests for BGE instructions
  * PC: Add tests for BLT instructions
  * PC: Add tests for BNE instructions
  * PC: Improve variable names
  * PC: Add tests for BEQ instructions
  * Memory: Add the StoreWord test to mem test main
  * Remove dead Makefile targets from the README
  * Memory: Add word store tests
  * Memory: Add tests for half word stores
  * Memory: Add store tests for bytes
  * Memory: Add load tests for different LSBs
  * Make install-deps.sh executable
  * Add tests for unsigned loads
  * Don't run non-exisitng targets in the Makefile
  * Change simulation flag to sim
  * PC Test: Minor code refactoring; tests are passing
  * Refix PcTester plus some changes to pc
  * Pc tester file fixed
  * Don't import module which are already imported
  * Refactor Load tests
  * Add tests for LW instructions
  * Add tests for load half instructions
  * Fix load bugs in memory
  * Improve the tests for the ALU
  * Add register file to test R0
  * Use Unit tests in all modules
  * Update travis build
  * Remove sudo and verilator deps
  * Don't install deps in travis. Only verilator
  * Need sudo to install deps in travis
  * Add travis build and deps install script
  * Add License headers and remove semi colons
  * Use config variables where applicable
  * Register File: get n registers and width from conf
  * Reorganize the code in the ALU
  * Fix bug where BEQs and BNEs weren't subtracting
  * Set simulation config variable
  * Bump Test Files
  * Add simulation success condition
  * PC starts from address 0x1000_0000
  * Change PC to byte addressable memory
  * Use a single memory for instruction and data
  * Memory: Refactor code to add more r/w ports easily
  * Fixed jump error
  * Add memory stall control logic to the core
  * Pc errors correction
  * Fix PC mem stall condition
  * Add memory stall signal to the PC
  * Update README with better instructions
  * Update LICENSE section in README
  * Fix a lot of mistakes I did while merging
  * Resolved commit conflicts
  * Pc branch corrections
  * Revamp testing framework
  * Add test repo
  * Fix small type bugs
  * Name changes for 2 in/out pins
  * Comment new control signals in the decoder
  * Stall core when memory takes more than one cycle
  * Corected the jump error
  * Changes to InstrMem, idecode, Pc and Core
  * Pipeline PC when used in the EX stage
  * Change instruction memory file name and Adept tester
  * Add Synchronous Instruction and Data Memory
  * Corrections
  * Fix a lot of decoder bugs
  * Add Adept tester with Fibonacci seq in instr-mem
  * Make the memory spec conformant
  * Add debug print
  * Fix compiler errors namely type casts to SInt/UInt
  * Add operand select control signals to the idecoder
  * Make memory byte addressable
  * Create LICENSE
  * Add Instruction Fetch stage
  * Add memory Module
  * Update README
  * Don't allow writes to register 0
  * Add decoder for R, S, B, U and J instr types
  * Connect the ALU, idecoder and regfile modules
  * Share interfaces among modules
  * Add cmp_flag to the ALU used only in Control instr
  * Fix bug caused by FIRRTL interpreter (issue 122)
  * Fix dumb mistake in two's complement conversion
  * Add tests for R-Type instructions (they all pass)
  * Implement final I-Type ALU ops SLTI and SLTIU
  * Add support for unsigned right shifts
  * Expand the ALU test Suite
  * Expand the tests in the ADDI instructions
  * All operations in the ALU are signed
  * Add makefile to generate verilog and test comps
  * Syntax corrections and generates verilog
  * Add implementation for 32I R and I type instrs
  * Start work on the Adepth RISC-V processor
