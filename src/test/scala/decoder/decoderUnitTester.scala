package adept.decoder

import chisel3.iotesters
import chisel3.iotesters.{ChiselFlatSpec, Driver, PeekPokeTester}

import adept.config.AdeptConfig

import adept.decoder.tests.imm._
import adept.decoder.tests.reg._
import adept.decoder.tests.br._
import adept.decoder.tests.store._
import adept.decoder.tests.load._

class DecoderTestBase(c: InstructionDecoder) extends PeekPokeTester(c) {
  val op_code = new OpCodes

  // Register and Immediate Operations
  val sll = Integer.parseInt("001", 2)
  val slt = Integer.parseInt("010", 2)
  val sltu = Integer.parseInt("011", 2)
  val xor = Integer.parseInt("100", 2)
  val or = Integer.parseInt("110", 2)
  val and = Integer.parseInt("111", 2)
  val sr = Integer.parseInt("101", 2)

  // Branch Operations
  val bne = Integer.parseInt("001", 2)
  val blt = Integer.parseInt("100", 2)
  val bge = Integer.parseInt("101", 2)
  val bltu = Integer.parseInt("110", 2)
  val bgeu = Integer.parseInt("111", 2)

  // Store Operations
  val sh = Integer.parseInt("001", 2)
  val sw = Integer.parseInt("010", 2)

  // Load Operations
  val lb = Integer.parseInt("000", 2)

  val funct7alu = Integer.parseInt("0100000", 2);

  def signExtension(imm: Int, nbits: Int) : Int = {
    if ((imm >> (nbits - 1)) == 1) {
      ((0xFFFFFFFF << nbits) | imm)
    } else {
      imm
    }
  }
}

class DecoderUnitTesterAll(e: InstructionDecoder) extends PeekPokeTester(e) {
    // Immediate Type Instructions
    new ADDI(e)
    new SLTI(e)
    new SLLI(e)
    new SLTIU(e)
    new XORI(e)
    new ORI(e)
    new ANDI(e)
    new SRLI(e)
    new SRAI(e)
    new LBRegular(e)
    new LoadTrap(e)

    // Register Type Instructions
    new ADD(e)
    new SUB(e)
    new SLL(e)
    new SLT(e)
    new SLTU(e)
    new XOR(e)
    new SRL(e)
    new SRA(e)
    new OR(e)
    new AND(e)

    // Branch Type Instructions
    new BEQ(e)
    new BNE(e)
    new BLT(e)
    new BGE(e)
    new BLTU(e)
    new BGEU(e)

    // Store Type Instructions
    new SB(e)
    new SH(e)
    new SW(e)
}

class DecoderTester extends ChiselFlatSpec {
  // Generate configuration
  val config = new AdeptConfig

  ///////////////////////////////////////////////////////////////////////////
  // Immediate Type Instructions
  ////////////////////////////////////////////////////////////////////////////
  "Decoder" should s"test ADDI instruction (with verilator)" in {
    Driver(() => new InstructionDecoder(config), "verilator") {
      e => new ADDI(e)
    } should be (true)
  }
  "Decoder" should s"test SLTI instruction (with verilator)" in {
    Driver(() => new InstructionDecoder(config), "verilator") {
      e => new SLTI(e)
    } should be (true)
  }
  "Decoder" should s"test SLTIU instruction (with verilator)" in {
    Driver(() => new InstructionDecoder(config), "verilator") {
      e => new SLTIU(e)
    } should be (true)
  }
  "Decoder" should s"test ORI instruction (with verilator)" in {
    Driver(() => new InstructionDecoder(config), "verilator") {
      e => new ORI(e)
    } should be (true)
  }
  "Decoder" should s"test XORI instruction (with verilator)" in {
    Driver(() => new InstructionDecoder(config), "verilator") {
      e => new XORI(e)
    } should be (true)
  }
  "Decoder" should s"test ANDI instruction (with verilator)" in {
    Driver(() => new InstructionDecoder(config), "verilator") {
      e => new ANDI(e)
    } should be (true)
  }
  "Decoder" should s"test SLLI instruction (with verilator)" in {
    Driver(() => new InstructionDecoder(config), "verilator") {
      e => new SLLI(e)
    } should be (true)
  }
  "Decoder" should s"test SRLI instruction (with verilator)" in {
    Driver(() => new InstructionDecoder(config), "verilator") {
      e => new SRLI(e)
    } should be (true)
  }
  "Decoder" should s"test SRAI instruction (with verilator)" in {
    Driver(() => new InstructionDecoder(config), "verilator") {
      e => new SRAI(e)
    } should be (true)
  }
  "Decoder" should s"test LB instruction (with verilator)" in {
    Driver(() => new InstructionDecoder(config), "verilator") {
      e => new LBRegular(e)
    } should be (true)
  }
  "Decoder" should s"test trap for load instructions (with verilator)" in {
    Driver(() => new InstructionDecoder(config), "verilator") {
      e => new LoadTrap(e)
    } should be (true)
  }

  ////////////////////////////////////////////////////////////////////////////
  // Register Type Instructions
  ////////////////////////////////////////////////////////////////////////////
  "Decoder" should s"test ADD instruction (with verilator)" in {
    Driver(() => new InstructionDecoder(config), "verilator") {
      e => new ADD(e)
    } should be (true)
  }
  "Decoder" should s"test SUB instruction (with verilator)" in {
    Driver(() => new InstructionDecoder(config), "verilator") {
      e => new SUB(e)
    } should be (true)
  }
  "Decoder" should s"test SLL instruction (with verilator)" in {
    Driver(() => new InstructionDecoder(config), "verilator") {
      e => new SLL(e)
    } should be (true)
  }
  "Decoder" should s"test SLT instruction (with verilator)" in {
    Driver(() => new InstructionDecoder(config), "verilator") {
      e => new SLT(e)
    } should be (true)
  }
  "Decoder" should s"test SLTU instruction (with verilator)" in {
    Driver(() => new InstructionDecoder(config), "verilator") {
      e => new SLTU(e)
    } should be (true)
  }
  "Decoder" should s"test XOR instruction (with verilator)" in {
    Driver(() => new InstructionDecoder(config), "verilator") {
      e => new XOR(e)
    } should be (true)
  }
  "Decoder" should s"test SRL instruction (with verilator)" in {
    Driver(() => new InstructionDecoder(config), "verilator") {
      e => new SRL(e)
    } should be (true)
  }
  "Decoder" should s"test SRA instruction (with verilator)" in {
    Driver(() => new InstructionDecoder(config), "verilator") {
      e => new SRA(e)
    } should be (true)
  }
  "Decoder" should s"test OR instruction (with verilator)" in {
    Driver(() => new InstructionDecoder(config), "verilator") {
      e => new OR(e)
    } should be (true)
  }
  "Decoder" should s"test AND instruction (with verilator)" in {
    Driver(() => new InstructionDecoder(config), "verilator") {
      e => new AND(e)
    } should be (true)
  }

  ////////////////////////////////////////////////////////////////////////////
  // Branch Type Instructions
  ////////////////////////////////////////////////////////////////////////////
  "Decoder" should s"test BEQ instructions (with verilator)" in {
    Driver(() => new InstructionDecoder(config), "verilator") {
      e => new BEQ(e)
    } should be (true)
  }
  "Decoder" should s"test BNE instructions (with verilator)" in {
    Driver(() => new InstructionDecoder(config), "verilator") {
      e => new BNE(e)
    } should be (true)
  }
  "Decoder" should s"test BLT instructions (with verilator)" in {
    Driver(() => new InstructionDecoder(config), "verilator") {
      e => new BLT(e)
    } should be (true)
  }
  "Decoder" should s"test BGE instructions (with verilator)" in {
    Driver(() => new InstructionDecoder(config), "verilator") {
      e => new BGE(e)
    } should be (true)
  }
  "Decoder" should s"test BLTU instructions (with verilator)" in {
    Driver(() => new InstructionDecoder(config), "verilator") {
      e => new BLTU(e)
    } should be (true)
  }
  "Decoder" should s"test BGEU instructions (with verilator)" in {
    Driver(() => new InstructionDecoder(config), "verilator") {
      e => new BGEU(e)
    } should be (true)
  }

  ///////////////////////////////////////////////////////////////////////////
  // Store Type Instructions
  ////////////////////////////////////////////////////////////////////////////
  "Decoder" should s"test SB instruction (with verilator)" in {
    Driver(() => new InstructionDecoder(config), "verilator") {
      e => new SB(e)
    } should be (true)
  }
  "Decoder" should s"test SH instruction (with verilator)" in {
    Driver(() => new InstructionDecoder(config), "verilator") {
      e => new SH(e)
    } should be (true)
  }
  "Decoder" should s"test SW instruction (with verilator)" in {
    Driver(() => new InstructionDecoder(config), "verilator") {
      e => new SW(e)
    } should be (true)
  }
}
