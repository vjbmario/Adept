package adept.decoder.tests.br

import chisel3.iotesters
import chisel3.iotesters.{ChiselFlatSpec, Driver, PeekPokeTester}

import adept.decoder._
import adept.alu.AluOps
import adept.core.AdeptControlSignals
import adept.pc.PcOps

////////////////////////////////////////////////
// Test Suite for Branch Type instructions
////////////////////////////////////////////////
class BEQ(c: InstructionDecoder) extends DecoderTestBase(c) {
 private def BEQ(rs1: Int, rs2: Int, imm: Int) {
    val instr = (((imm & 2048) << 20) | ((imm & 1008) << 21) | ((31 & rs2) << 20) |
                ((31 & rs1) << 15) | ((imm & 15) << 8) | ((imm & 1024) >> 3) |
                 op_code.Branches.litValue())
    val offset = signExtension((imm << 1), 13)

    poke(c.io.stall_reg, false)
    poke(c.io.basic.instruction, instr)

    step(1)

    expect(c.io.basic.out.registers.rs1_sel, rs1)
    expect(c.io.basic.out.registers.rs2_sel, rs2)
    expect(c.io.basic.out.pc.br_offset, offset)
    expect(c.io.basic.out.trap, 0)
    expect(c.io.basic.out.pc.op, PcOps.beq)
    expect(c.io.basic.out.alu.op, AluOps.sub)
    expect(c.io.basic.out.sel_operand_a, AdeptControlSignals.sel_oper_A_rs1)
    expect(c.io.basic.out.sel_operand_b, AdeptControlSignals.sel_oper_B_rs2)
  }

  for (i <- 0 until 100) {
    val rs1 = rnd.nextInt(32)
    val imm = -2048 + rnd.nextInt(4096)
    val rs2  = rnd.nextInt(32)

    BEQ(rs1, rs2, imm)
  }
}

class BNE(c: InstructionDecoder) extends DecoderTestBase(c) {
 private def BNE(rs1: Int, rs2: Int, imm: Int) {
    val instr = (((imm & 2048) << 20) | ((imm & 1008) << 21) |
                 ((31 & rs2) << 20) | ((31 & rs1) << 15) | bne << 12 |
                 ((imm & 15) << 8) | ((imm & 1024) >> 3) |
                 op_code.Branches.litValue())
    val offset = signExtension((imm << 1), 13)

    poke(c.io.stall_reg, false)
    poke(c.io.basic.instruction, instr)

    step(1)

    expect(c.io.basic.out.registers.rs1_sel, rs1)
    expect(c.io.basic.out.registers.rs2_sel, rs2)
    expect(c.io.basic.out.pc.br_offset, offset)
    expect(c.io.basic.out.trap, 0)
    expect(c.io.basic.out.pc.op, PcOps.bne)
    expect(c.io.basic.out.alu.op, AluOps.sub)
    expect(c.io.basic.out.sel_operand_a, AdeptControlSignals.sel_oper_A_rs1)
    expect(c.io.basic.out.sel_operand_b, AdeptControlSignals.sel_oper_B_rs2)
  }

  for (i <- 0 until 100) {
    val rs1 = rnd.nextInt(32)
    val imm = -2048 + rnd.nextInt(4096)
    val rs2  = rnd.nextInt(32)

    BNE(rs1, rs2, imm)
  }
}

class BLT(c: InstructionDecoder) extends DecoderTestBase(c) {
 private def BLT(rs1: Int, rs2: Int, imm: Int) {
    val instr = (((imm & 2048) << 20) | ((imm & 1008) << 21) |
                 ((31 & rs2) << 20) | ((31 & rs1) << 15) | blt << 12 |
                 ((imm & 15) << 8) | ((imm & 1024) >> 3) |
                 op_code.Branches.litValue())
    val offset = signExtension((imm << 1), 13)

    poke(c.io.stall_reg, false)
    poke(c.io.basic.instruction, instr)

    step(1)

    expect(c.io.basic.out.registers.rs1_sel, rs1)
    expect(c.io.basic.out.registers.rs2_sel, rs2)
    expect(c.io.basic.out.pc.br_offset, offset)
    expect(c.io.basic.out.trap, 0)
    expect(c.io.basic.out.pc.op, PcOps.blt)
    expect(c.io.basic.out.alu.op, AluOps.slt)
    expect(c.io.basic.out.sel_operand_a, AdeptControlSignals.sel_oper_A_rs1)
    expect(c.io.basic.out.sel_operand_b, AdeptControlSignals.sel_oper_B_rs2)
  }

  for (i <- 0 until 100) {
    val rs1 = rnd.nextInt(32)
    val imm = -2048 + rnd.nextInt(4096)
    val rs2  = rnd.nextInt(32)

    BLT(rs1, rs2, imm)
  }
}

class BGE(c: InstructionDecoder) extends DecoderTestBase(c) {
 private def BGE(rs1: Int, rs2: Int, imm: Int) {
    val instr = (((imm & 2048) << 20) | ((imm & 1008) << 21) |
                 ((31 & rs2) << 20) | ((31 & rs1) << 15) | bge << 12 |
                 ((imm & 15) << 8) | ((imm & 1024) >> 3) |
                 op_code.Branches.litValue())
    val offset = signExtension((imm << 1), 13)

    poke(c.io.stall_reg, false)
    poke(c.io.basic.instruction, instr)

    step(1)

    expect(c.io.basic.out.registers.rs1_sel, rs1)
    expect(c.io.basic.out.registers.rs2_sel, rs2)
    expect(c.io.basic.out.pc.br_offset, offset)
    expect(c.io.basic.out.trap, 0)
    expect(c.io.basic.out.pc.op, PcOps.bge)
    expect(c.io.basic.out.alu.op, AluOps.slt)
    expect(c.io.basic.out.sel_operand_a, AdeptControlSignals.sel_oper_A_rs1)
    expect(c.io.basic.out.sel_operand_b, AdeptControlSignals.sel_oper_B_rs2)
  }

  for (i <- 0 until 100) {
    val rs1 = rnd.nextInt(32)
    val imm = -2048 + rnd.nextInt(4096)
    val rs2  = rnd.nextInt(32)

    BGE(rs1, rs2, imm)
  }
}

class BLTU(c: InstructionDecoder) extends DecoderTestBase(c) {
 private def BLTU(rs1: Int, rs2: Int, imm: Int) {
    val instr = (((imm & 2048) << 20) | ((imm & 1008) << 21) |
                 ((31 & rs2) << 20) | ((31 & rs1) << 15) | bltu << 12 |
                 ((imm & 15) << 8) | ((imm & 1024) >> 3) |
                 op_code.Branches.litValue())
    val offset = signExtension((imm << 1), 13)

    poke(c.io.stall_reg, false)
    poke(c.io.basic.instruction, instr)

    step(1)

    expect(c.io.basic.out.registers.rs1_sel, rs1)
    expect(c.io.basic.out.registers.rs2_sel, rs2)
    expect(c.io.basic.out.pc.br_offset, offset)
    expect(c.io.basic.out.trap, 0)
    expect(c.io.basic.out.pc.op, PcOps.bltu)
    expect(c.io.basic.out.alu.op, AluOps.sltu)
    expect(c.io.basic.out.sel_operand_a, AdeptControlSignals.sel_oper_A_rs1)
    expect(c.io.basic.out.sel_operand_b, AdeptControlSignals.sel_oper_B_rs2)
  }

  for (i <- 0 until 100) {
    val rs1 = rnd.nextInt(32)
    val imm = -2048 + rnd.nextInt(4096)
    val rs2  = rnd.nextInt(32)

    BLTU(rs1, rs2, imm)
  }
}

class BGEU(c: InstructionDecoder) extends DecoderTestBase(c) {
 private def BGEU(rs1: Int, rs2: Int, imm: Int) {
    val instr = (((imm & 2048) << 20) | ((imm & 1008) << 21) |
                 ((31 & rs2) << 20) | ((31 & rs1) << 15) | bgeu << 12 |
                 ((imm & 15) << 8) | ((imm & 1024) >> 3) |
                 op_code.Branches.litValue())
    val offset = signExtension((imm << 1), 13)

    poke(c.io.stall_reg, false)
    poke(c.io.basic.instruction, instr)

    step(1)

    expect(c.io.basic.out.registers.rs1_sel, rs1)
    expect(c.io.basic.out.registers.rs2_sel, rs2)
    expect(c.io.basic.out.pc.br_offset, offset)
    expect(c.io.basic.out.trap, 0)
    expect(c.io.basic.out.pc.op, PcOps.bgeu)
    expect(c.io.basic.out.alu.op, AluOps.sltu)
    expect(c.io.basic.out.sel_operand_a, AdeptControlSignals.sel_oper_A_rs1)
    expect(c.io.basic.out.sel_operand_b, AdeptControlSignals.sel_oper_B_rs2)
  }

  for (i <- 0 until 100) {
    val rs1 = rnd.nextInt(32)
    val imm = -2048 + rnd.nextInt(4096)
    val rs2  = rnd.nextInt(32)

    BGEU(rs1, rs2, imm)
  }
}
