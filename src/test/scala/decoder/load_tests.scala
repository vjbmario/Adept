package adept.decoder.tests.load

import chisel3.iotesters
import chisel3.iotesters.{ChiselFlatSpec, Driver, PeekPokeTester}

import adept.decoder._
import adept.mem.MemOps
import adept.alu.AluOps
import adept.core.AdeptControlSignals

////////////////////////////////////////////////
// Test Suite for Load Type instructions
////////////////////////////////////////////////
class LoadBase(c: InstructionDecoder) extends DecoderTestBase(c) {
  protected def setLoadSignals(rs1: Int, rsd: Int, imm: Int, op: Int) {
    val instr = ((imm << 20) | ((31 & rs1) << 15) | (op << 12) |
                 ((31 & rsd) << 7) | op_code.Loads.litValue())

    poke(c.io.stall_reg, false)
    poke(c.io.basic.instruction, instr)
  }

  protected def expectLoadSignals(rs1: Int, rsd: Int, imm: Int, 
                                  op: BigInt, trap: Int) {
    val new_imm = signExtension(imm, 12)

    expect(c.io.basic.out.registers.we, true)
    expect(c.io.basic.out.mem.en, true)
    if (trap == 0) {
      expect(c.io.basic.out.mem.op, op)
    }
    expect(c.io.basic.out.registers.rs1_sel, rs1)
    expect(c.io.basic.out.registers.rsd_sel, rsd)
    expect(c.io.basic.out.immediate, new_imm)
    expect(c.io.basic.out.alu.op, AluOps.add)
    expect(c.io.basic.out.trap, trap)
    expect(c.io.basic.out.sel_rf_wb, AdeptControlSignals.result_mem)
    expect(c.io.basic.out.sel_operand_a, AdeptControlSignals.sel_oper_A_rs1)
    expect(c.io.basic.out.sel_operand_b, AdeptControlSignals.sel_oper_B_imm)
  }
}

class LBRegular(c: InstructionDecoder) extends LoadBase(c) {
  for (i <- 0 until 100) {
    val rs1 = rnd.nextInt(32)
    val rsd = rnd.nextInt(32)
    val imm = -2048 + rnd.nextInt(4096)

    setLoadSignals(rs1, rsd, imm, lb)
    step(1)
    expectLoadSignals(rs1, rsd, imm, MemOps.lb.litValue, 0)
  }
}

class LoadTrap(c: InstructionDecoder) extends LoadBase(c) {
  val rs1 = rnd.nextInt(32)
  val rsd = rnd.nextInt(32)
  val imm = -2048 + rnd.nextInt(4096)
  
  //Values 3, 6 and 7 are the funct3 op that doesn't match the instruction spec
  setLoadSignals(rs1, rsd, imm, 3)
  step(1)
  expectLoadSignals(rs1, rsd, imm, 3, 1)
  
  setLoadSignals(rs1, rsd, imm, 6)
  step(1)
  expectLoadSignals(rs1, rsd, imm, 6, 1)
  
  setLoadSignals(rs1, rsd, imm, 7)
  step(1)
  expectLoadSignals(rs1, rsd, imm, 7, 1)
}
