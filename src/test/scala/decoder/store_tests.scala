package adept.decoder.tests.store

import chisel3.iotesters
import chisel3.iotesters.{ChiselFlatSpec, Driver, PeekPokeTester}

import adept.decoder._
import adept.mem.MemOps
import adept.core.AdeptControlSignals

////////////////////////////////////////////////
// Test Suite for Store Type instructions
////////////////////////////////////////////////
class SB(c: InstructionDecoder) extends DecoderTestBase(c) {
  private def SB(rs1: Int, rs2: Int, imm: Int) {
    val instr = (((127 & (imm >> 5)) << 25) | ((31 & rs2) << 20) |
                 ((31 & rs1) << 15) |
                 ((31 & imm) << 7) | op_code.Stores.litValue())
    val new_imm = signExtension(imm, 12)

    poke(c.io.stall_reg, false)
    poke(c.io.basic.instruction, instr)

    step(1)

    expect(c.io.basic.out.mem.we, true)
    expect(c.io.basic.out.mem.en, true)
    expect(c.io.basic.out.mem.op, MemOps.sb)
    expect(c.io.basic.out.registers.rs1_sel, rs1)
    expect(c.io.basic.out.registers.rs2_sel, rs2)
    expect(c.io.basic.out.immediate, new_imm)
    expect(c.io.basic.out.trap, 0)
    expect(c.io.basic.out.sel_operand_a, AdeptControlSignals.sel_oper_A_rs1)
    expect(c.io.basic.out.sel_operand_b, AdeptControlSignals.sel_oper_B_imm)
  }

  for (i <- 0 until 100) {
    val rs1 = rnd.nextInt(32)
    val rs2 = rnd.nextInt(32)
    val imm = -2048 + rnd.nextInt(4096)

    SB(rs1, rs2, imm)
  }
}

class SH(c: InstructionDecoder) extends DecoderTestBase(c) {
  private def SH(rs1: Int, rs2: Int, imm: Int) {
    val instr = (((127 & (imm >> 5)) << 25) | ((31 & rs2) << 20) |
                 ((31 & rs1) << 15) | (sh << 12) |
                 ((31 & imm) << 7) | op_code.Stores.litValue())
    val new_imm = signExtension(imm, 12)

    poke(c.io.stall_reg, false)
    poke(c.io.basic.instruction, instr)

    step(1)

    expect(c.io.basic.out.mem.we, true)
    expect(c.io.basic.out.mem.en, true)
    expect(c.io.basic.out.mem.op, MemOps.sh)
    expect(c.io.basic.out.registers.rs1_sel, rs1)
    expect(c.io.basic.out.registers.rs2_sel, rs2)
    expect(c.io.basic.out.immediate, new_imm)
    expect(c.io.basic.out.trap, 0)
    expect(c.io.basic.out.sel_operand_a, AdeptControlSignals.sel_oper_A_rs1)
    expect(c.io.basic.out.sel_operand_b, AdeptControlSignals.sel_oper_B_imm)
  }

  for (i <- 0 until 100) {
    val rs1 = rnd.nextInt(32)
    val rs2 = rnd.nextInt(32)
    val imm = -2048 + rnd.nextInt(4096)

    SH(rs1, rs2, imm)
  }
}

class SW(c: InstructionDecoder) extends DecoderTestBase(c) {
  private def SW(rs1: Int, rs2: Int, imm: Int) {
    val instr = (((127 & (imm >> 5)) << 25) | ((31 & rs2) << 20) |
                 ((31 & rs1) << 15) | (sw << 12) |
                 ((31 & imm) << 7) | op_code.Stores.litValue())
    val new_imm = signExtension(imm, 12)

    poke(c.io.stall_reg, false)
    poke(c.io.basic.instruction, instr)

    step(1)

    expect(c.io.basic.out.mem.we, true)
    expect(c.io.basic.out.mem.en, true)
    expect(c.io.basic.out.mem.op, MemOps.sw)
    expect(c.io.basic.out.registers.rs1_sel, rs1)
    expect(c.io.basic.out.registers.rs2_sel, rs2)
    expect(c.io.basic.out.immediate, new_imm)
    expect(c.io.basic.out.trap, 0)
    expect(c.io.basic.out.sel_operand_a, AdeptControlSignals.sel_oper_A_rs1)
    expect(c.io.basic.out.sel_operand_b, AdeptControlSignals.sel_oper_B_imm)
  }

  for (i <- 0 until 100) {
    val rs1 = rnd.nextInt(32)
    val rs2 = rnd.nextInt(32)
    val imm = -2048 + rnd.nextInt(4096)

    SW(rs1, rs2, imm)
  }
}
