package adept.decoder.tests.reg

import chisel3.iotesters
import chisel3.iotesters.{ChiselFlatSpec, Driver, PeekPokeTester}

import adept.decoder._
import adept.alu.AluOps
import adept.core.AdeptControlSignals

////////////////////////////////////////////////
// Test Suite for Register Type instructions
////////////////////////////////////////////////
class ADD(c: InstructionDecoder) extends DecoderTestBase(c) {
  private def ADD(rs1: Int, rs2: Int, imm: Int, rd: Int) {
    val instr = (((127 & imm) << 25) | ((31 & rs2) << 20) |
                 ((31 & rs1) << 15) | ((31 & rd) << 7) | op_code.Registers.litValue())
    val new_imm = signExtension(imm, 7)
    val trap = if ((imm == 0) || (imm == funct7alu)) {
                 0
               } else {
                 1
               }
    poke(c.io.stall_reg, false)
    poke(c.io.basic.instruction, instr)

    step(1)

    expect(c.io.basic.out.registers.we, true)
    expect(c.io.basic.out.registers.rsd_sel, rd)
    expect(c.io.basic.out.registers.rs1_sel, rs1)
    expect(c.io.basic.out.registers.rs2_sel, rs2)
    expect(c.io.basic.out.immediate, new_imm)
    expect(c.io.basic.out.trap, trap)
    if (imm == 0) {
      expect(c.io.basic.out.alu.op, AluOps.add)
    }
    expect(c.io.basic.out.sel_rf_wb, AdeptControlSignals.result_alu)
    expect(c.io.basic.out.sel_operand_a, AdeptControlSignals.sel_oper_A_rs1)
    expect(c.io.basic.out.sel_operand_b, AdeptControlSignals.sel_oper_B_rs2)
  }

  for (i <- 0 until 100) {
    val rs1 = rnd.nextInt(32)
    val rs2 = rnd.nextInt(32)
    val random = rnd.nextInt(128)
    val imm = if (random % 2 == 0) {
                0
              } else if (random % 5 == 0) {
                funct7alu
              } else {
                random
              }
    val rd  = rnd.nextInt(32)

    ADD(rs1, rs2, imm, rd)
  }
}

class SUB(c: InstructionDecoder) extends DecoderTestBase(c) {
  private def SUB(rs1: Int, rs2: Int, imm: Int, rd: Int) {
    val instr = (((127 & imm) << 25) | ((31 & rs2) << 20) |
                 ((31 & rs1) << 15) | ((31 & rd) << 7) | op_code.Registers.litValue())
    val new_imm = if ((imm >> 6) == 1) {
                    ((0xFFFFFFF << 7) | imm)
                  } else {
                    imm
                  }
    val trap = if ((imm == 0) || (imm == funct7alu)) {
                 0
               } else {
                 1
               }
    poke(c.io.stall_reg, false)
    poke(c.io.basic.instruction, instr)

    step(1)

    expect(c.io.basic.out.registers.we, true)
    expect(c.io.basic.out.registers.rsd_sel, rd)
    expect(c.io.basic.out.registers.rs1_sel, rs1)
    expect(c.io.basic.out.registers.rs2_sel, rs2)
    expect(c.io.basic.out.immediate, new_imm)
    expect(c.io.basic.out.trap, trap)
    if (imm == funct7alu) {
      expect(c.io.basic.out.alu.op, AluOps.sub)
    }
    expect(c.io.basic.out.sel_rf_wb, AdeptControlSignals.result_alu)
    expect(c.io.basic.out.sel_operand_a, AdeptControlSignals.sel_oper_A_rs1)
    expect(c.io.basic.out.sel_operand_b, AdeptControlSignals.sel_oper_B_rs2)
  }

  for (i <- 0 until 100) {
    val rs1 = rnd.nextInt(32)
    val rs2 = rnd.nextInt(32)
    val random = rnd.nextInt(128)
    val imm = if (random % 2 == 0) {
                funct7alu
              } else if (random % 5 == 0) {
                0
              } else {
                random
              }
    val rd  = rnd.nextInt(32)

    SUB(rs1, rs2, imm, rd)
  }
}

class SLL(c: InstructionDecoder) extends DecoderTestBase(c) {
  private def SLL(rs1: Int, rs2: Int, imm: Int, rd: Int) {
    val instr = (((127 & imm) << 25) | ((31 & rs2) << 20) |
                 ((31 & rs1) << 15) | (sll << 12) |
                 ((31 & rd) << 7) | op_code.Registers.litValue())
    val new_imm = signExtension(imm, 7)
    val trap = if (imm == 0) {
                 0
               } else {
                 1
               }
    poke(c.io.stall_reg, false)
    poke(c.io.basic.instruction, instr)

    step(1)

    expect(c.io.basic.out.registers.we, true)
    expect(c.io.basic.out.registers.rsd_sel, rd)
    expect(c.io.basic.out.registers.rs1_sel, rs1)
    expect(c.io.basic.out.registers.rs2_sel, rs2)
    expect(c.io.basic.out.immediate, new_imm)
    expect(c.io.basic.out.trap, trap)
    expect(c.io.basic.out.alu.op, AluOps.sll)
    expect(c.io.basic.out.sel_rf_wb, AdeptControlSignals.result_alu)
    expect(c.io.basic.out.sel_operand_a, AdeptControlSignals.sel_oper_A_rs1)
    expect(c.io.basic.out.sel_operand_b, AdeptControlSignals.sel_oper_B_rs2)
  }

  for (i <- 0 until 100) {
    val rs1 = rnd.nextInt(32)
    val rs2 = rnd.nextInt(32)
    val random = rnd.nextInt(128)
    val imm = if (random % 2 == 0) {
                0
              } else if (random % 5 == 0) {
                funct7alu
              } else {
                random
              }
    val rd  = rnd.nextInt(32)

    SLL(rs1, rs2, imm, rd)
  }
}

class SLT(c: InstructionDecoder) extends DecoderTestBase(c) {
  private def SLT(rs1: Int, rs2: Int, imm: Int, rd: Int) {
    val instr = (((127 & imm) << 25) | ((31 & rs2) << 20) |
                 ((31 & rs1) << 15) | (slt << 12) |
                 ((31 & rd) << 7) | op_code.Registers.litValue())
    val new_imm = signExtension(imm, 7)
    val trap = if (imm == 0) {
                 0
               } else {
                 1
               }
    poke(c.io.stall_reg, false)
    poke(c.io.basic.instruction, instr)

    step(1)

    expect(c.io.basic.out.registers.we, true)
    expect(c.io.basic.out.registers.rsd_sel, rd)
    expect(c.io.basic.out.registers.rs1_sel, rs1)
    expect(c.io.basic.out.registers.rs2_sel, rs2)
    expect(c.io.basic.out.immediate, new_imm)
    expect(c.io.basic.out.trap, trap)
    expect(c.io.basic.out.alu.op, AluOps.slt)
    expect(c.io.basic.out.sel_rf_wb, AdeptControlSignals.result_alu)
    expect(c.io.basic.out.sel_operand_a, AdeptControlSignals.sel_oper_A_rs1)
    expect(c.io.basic.out.sel_operand_b, AdeptControlSignals.sel_oper_B_rs2)
  }

  for (i <- 0 until 100) {
    val rs1 = rnd.nextInt(32)
    val rs2 = rnd.nextInt(32)
    val random = rnd.nextInt(128)
    val imm = if (random % 2 == 0) {
                0
              } else if (random % 5 == 0) {
                funct7alu
              } else {
                random
              }
    val rd  = rnd.nextInt(32)

    SLT(rs1, rs2, imm, rd)
  }
}

class SLTU(c: InstructionDecoder) extends DecoderTestBase(c) {
  private def SLTU(rs1: Int, rs2: Int, imm: Int, rd: Int) {
    val instr = (((127 & imm) << 25) | ((31 & rs2) << 20) |
                 ((31 & rs1) << 15) | (sltu << 12) |
                 ((31 & rd) << 7) | op_code.Registers.litValue())
    val new_imm = signExtension(imm, 7)
    val trap = if (imm == 0) {
                 0
               } else {
                 1
               }
    poke(c.io.stall_reg, false)
    poke(c.io.basic.instruction, instr)

    step(1)

    expect(c.io.basic.out.registers.we, true)
    expect(c.io.basic.out.registers.rsd_sel, rd)
    expect(c.io.basic.out.registers.rs1_sel, rs1)
    expect(c.io.basic.out.registers.rs2_sel, rs2)
    expect(c.io.basic.out.immediate, new_imm)
    expect(c.io.basic.out.trap, trap)
    expect(c.io.basic.out.alu.op, AluOps.sltu)
    expect(c.io.basic.out.sel_rf_wb, AdeptControlSignals.result_alu)
    expect(c.io.basic.out.sel_operand_a, AdeptControlSignals.sel_oper_A_rs1)
    expect(c.io.basic.out.sel_operand_b, AdeptControlSignals.sel_oper_B_rs2)
  }

  for (i <- 0 until 100) {
    val rs1 = rnd.nextInt(32)
    val rs2 = rnd.nextInt(32)
    val random = rnd.nextInt(128)
    val imm = if (random % 2 == 0) {
                0
              } else if (random % 5 == 0) {
                funct7alu
              } else {
                random
              }
    val rd  = rnd.nextInt(32)

    SLTU(rs1, rs2, imm, rd)
  }
}

class XOR(c: InstructionDecoder) extends DecoderTestBase(c) {
  private def XOR(rs1: Int, rs2: Int, imm: Int, rd: Int) {
    val instr = (((127 & imm) << 25) | ((31 & rs2) << 20) |
                 ((31 & rs1) << 15) | (xor << 12) |
                 ((31 & rd) << 7) | op_code.Registers.litValue())
    val new_imm = signExtension(imm, 7)
    val trap = if (imm == 0) {
                 0
               } else {
                 1
               }
    poke(c.io.stall_reg, false)
    poke(c.io.basic.instruction, instr)

    step(1)

    expect(c.io.basic.out.registers.we, true)
    expect(c.io.basic.out.registers.rsd_sel, rd)
    expect(c.io.basic.out.registers.rs1_sel, rs1)
    expect(c.io.basic.out.registers.rs2_sel, rs2)
    expect(c.io.basic.out.immediate, new_imm)
    expect(c.io.basic.out.trap, trap)
    expect(c.io.basic.out.alu.op, AluOps.xor)
    expect(c.io.basic.out.sel_rf_wb, AdeptControlSignals.result_alu)
    expect(c.io.basic.out.sel_operand_a, AdeptControlSignals.sel_oper_A_rs1)
    expect(c.io.basic.out.sel_operand_b, AdeptControlSignals.sel_oper_B_rs2)
  }

  for (i <- 0 until 100) {
    val rs1 = rnd.nextInt(32)
    val rs2 = rnd.nextInt(32)
    val random = rnd.nextInt(128)
    val imm = if (random % 2 == 0) {
                0
              } else if (random % 5 == 0) {
                funct7alu
              } else {
                random
              }
    val rd  = rnd.nextInt(32)

    XOR(rs1, rs2, imm, rd)
  }
}

class SRL(c: InstructionDecoder) extends DecoderTestBase(c) {
  private def SRL(rs1: Int, rs2: Int, imm: Int, rd: Int) {
    val instr = (((127 & imm) << 25) | ((31 & rs2) << 20) |
                 ((31 & rs1) << 15) | (sr << 12) |
                 ((31 & rd) << 7) | op_code.Registers.litValue())
    val new_imm = signExtension(imm, 7)
    val trap = if ((imm == 0) || (imm == funct7alu)) {
                 0
               } else {
                 1
               }
    poke(c.io.stall_reg, false)
    poke(c.io.basic.instruction, instr)

    step(1)

    expect(c.io.basic.out.registers.we, true)
    expect(c.io.basic.out.registers.rsd_sel, rd)
    expect(c.io.basic.out.registers.rs1_sel, rs1)
    expect(c.io.basic.out.registers.rs2_sel, rs2)
    expect(c.io.basic.out.immediate, new_imm)
    expect(c.io.basic.out.trap, trap)
    if (imm == 0) {
      expect(c.io.basic.out.alu.op, AluOps.srl)
    }
    expect(c.io.basic.out.sel_rf_wb, AdeptControlSignals.result_alu)
    expect(c.io.basic.out.sel_operand_a, AdeptControlSignals.sel_oper_A_rs1)
    expect(c.io.basic.out.sel_operand_b, AdeptControlSignals.sel_oper_B_rs2)
  }

  for (i <- 0 until 100) {
    val rs1 = rnd.nextInt(32)
    val rs2 = rnd.nextInt(32)
    val random = rnd.nextInt(128)
    val imm = if (random % 2 == 0) {
                0
              } else if (random % 5 == 0) {
                funct7alu
              } else {
                random
              }
    val rd  = rnd.nextInt(32)

    SRL(rs1, rs2, imm, rd)
  }
}

class SRA(c: InstructionDecoder) extends DecoderTestBase(c) {
  private def SRA(rs1: Int, rs2: Int, imm: Int, rd: Int) {
    val instr = (((127 & imm) << 25) | ((31 & rs2) << 20) |
                 ((31 & rs1) << 15) | (sr << 12) |
                 ((31 & rd) << 7) | op_code.Registers.litValue())
    val new_imm = if ((imm >> 6) == 1) {
                    ((0xFFFFFFF << 7) | imm)
                  } else {
                    imm
                  }
    val trap = if ((imm == 0) || (imm == funct7alu)) {
                 0
               } else {
                 1
               }
    poke(c.io.stall_reg, false)
    poke(c.io.basic.instruction, instr)

    step(1)

    expect(c.io.basic.out.registers.we, true)
    expect(c.io.basic.out.registers.rsd_sel, rd)
    expect(c.io.basic.out.registers.rs1_sel, rs1)
    expect(c.io.basic.out.registers.rs2_sel, rs2)
    expect(c.io.basic.out.immediate, new_imm)
    expect(c.io.basic.out.trap, trap)
    if (imm == funct7alu) {
      expect(c.io.basic.out.alu.op, AluOps.sra)
    }
    expect(c.io.basic.out.sel_rf_wb, AdeptControlSignals.result_alu)
    expect(c.io.basic.out.sel_operand_a, AdeptControlSignals.sel_oper_A_rs1)
    expect(c.io.basic.out.sel_operand_b, AdeptControlSignals.sel_oper_B_rs2)
  }

  for (i <- 0 until 100) {
    val rs1 = rnd.nextInt(32)
    val rs2 = rnd.nextInt(32)
    val random = rnd.nextInt(128)
    val imm = if (random % 2 == 0) {
                funct7alu
              } else if (random % 5 == 0) {
                0
              } else {
                random
              }
    val rd  = rnd.nextInt(32)

    SRA(rs1, rs2, imm, rd)
  }
}

class OR(c: InstructionDecoder) extends DecoderTestBase(c) {
  private def OR(rs1: Int, rs2: Int, imm: Int, rd: Int) {
    val instr = (((127 & imm) << 25) | ((31 & rs2) << 20) |
                 ((31 & rs1) << 15) | (or << 12) |
                 ((31 & rd) << 7) | op_code.Registers.litValue())
    val new_imm = signExtension(imm, 7)
    val trap = if (imm == 0) {
                 0
               } else {
                 1
               }
    poke(c.io.stall_reg, false)
    poke(c.io.basic.instruction, instr)

    step(1)

    expect(c.io.basic.out.registers.we, true)
    expect(c.io.basic.out.registers.rsd_sel, rd)
    expect(c.io.basic.out.registers.rs1_sel, rs1)
    expect(c.io.basic.out.registers.rs2_sel, rs2)
    expect(c.io.basic.out.immediate, new_imm)
    expect(c.io.basic.out.trap, trap)
    expect(c.io.basic.out.alu.op, AluOps.or)
    expect(c.io.basic.out.sel_rf_wb, AdeptControlSignals.result_alu)
    expect(c.io.basic.out.sel_operand_a, AdeptControlSignals.sel_oper_A_rs1)
    expect(c.io.basic.out.sel_operand_b, AdeptControlSignals.sel_oper_B_rs2)
  }

  for (i <- 0 until 100) {
    val rs1 = rnd.nextInt(32)
    val rs2 = rnd.nextInt(32)
    val random = rnd.nextInt(128)
    val imm = if (random % 2 == 0) {
                0
              } else if (random % 5 == 0) {
                funct7alu
              } else {
                random
              }
    val rd  = rnd.nextInt(32)

    OR(rs1, rs2, imm, rd)
  }
}

class AND(c: InstructionDecoder) extends DecoderTestBase(c) {
  private def AND(rs1: Int, rs2: Int, imm: Int, rd: Int) {
    val instr = (((127 & imm) << 25) | ((31 & rs2) << 20) |
                 ((31 & rs1) << 15) | (and << 12) |
                 ((31 & rd) << 7) | op_code.Registers.litValue())
    val new_imm = signExtension(imm, 7)
    val trap = if (imm == 0) {
                 0
               } else {
                 1
               }
    poke(c.io.stall_reg, false)
    poke(c.io.basic.instruction, instr)

    step(1)

    expect(c.io.basic.out.registers.we, true)
    expect(c.io.basic.out.registers.rsd_sel, rd)
    expect(c.io.basic.out.registers.rs1_sel, rs1)
    expect(c.io.basic.out.registers.rs2_sel, rs2)
    expect(c.io.basic.out.immediate, new_imm)
    expect(c.io.basic.out.trap, trap)
    expect(c.io.basic.out.alu.op, AluOps.and)
    expect(c.io.basic.out.sel_rf_wb, AdeptControlSignals.result_alu)
    expect(c.io.basic.out.sel_operand_a, AdeptControlSignals.sel_oper_A_rs1)
    expect(c.io.basic.out.sel_operand_b, AdeptControlSignals.sel_oper_B_rs2)
  }

  for (i <- 0 until 100) {
    val rs1 = rnd.nextInt(32)
    val rs2 = rnd.nextInt(32)
    val random = rnd.nextInt(128)
    val imm = if (random % 2 == 0) {
                0
              } else if (random % 5 == 0) {
                funct7alu
              } else {
                random
              }
    val rd  = rnd.nextInt(32)

    AND(rs1, rs2, imm, rd)
  }
}
