#!/usr/bin/env bash

if [ -z "${RISCV+x}" ]; then
    echo "The RISCV environment variable is not set. Please set it and rerun script."
    exit 1
fi

# Create log folder
mkdir -p logs

TEST_FILE_FOLDER=Adept-TestFiles/instructions
HEXS=$TEST_FILE_FOLDER/hex
RESULTS_FOLDER=$TEST_FILE_FOLDER/results/
TOP=$(pwd)

cd $TEST_FILE_FOLDER && make clean && make
cd "$TOP"

for test in $HEXS/*; do
    TEST_NAME=${test##*/}
    LOG_FOLDER=logs/"$TEST_NAME"_log
    mkdir -p "$LOG_FOLDER"

    echo "Testing $TEST_NAME instruction"

    OUTPUT_TEST_FILE="$LOG_FOLDER/verilator_$test_$(date +%d-%m-%Y)"
    OUTPUT_RESULT_FILE="$LOG_FOLDER/verilator_result_$TEST_NAME_$(date +%d-%m-%Y)"

    # Since the verilator tests sometimes don't print the complete register
    # dump, we check the validity of the output before confirming if the
    # registers contain the expected values.
    CORRECT_OUTPUT=0
    while [ $CORRECT_OUTPUT = 0 ]; do
        # Run test in verilator
        make test-verilator PROG="$test" > "$OUTPUT_TEST_FILE"
        # Cut the log and take just the output that we want
        if grep -q 'Trap' $LOG_FOLDER/verilator_$test_$(date +%d-%m-%Y); then
            tac "$OUTPUT_TEST_FILE" | grep "Trap" -m 1 -B33 | sed 's/\[.*\] \[.*\] //g' | tac > "$OUTPUT_RESULT_FILE"
        else
            tac "$OUTPUT_TEST_FILE" | grep "PC = " -m 1 -B32 | sed 's/\[.*\] \[.*\] //g' | tac > "$OUTPUT_RESULT_FILE"
        fi

        # Validate output
        grep -q "Enabling waves.." < "$OUTPUT_RESULT_FILE"
        if [ $? = 1 ]; then
            CORRECT_OUTPUT=1
        else
            CORRECT_OUTPUT=0
        fi
    done

    diff -w "$OUTPUT_RESULT_FILE" "$RESULTS_FOLDER/${TEST_NAME%.hex}/verilator"
done
